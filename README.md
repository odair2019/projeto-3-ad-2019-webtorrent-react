# Projeto 3-AD-2019-WebTorrent-React

## Tutorial  WebTorrent com React

## Autor: Odair Xavier

![](http://i.gzn.jp/img/2016/04/16/webtorrent-desktop/top_00.png)

# Introdução

O cliente de torrent streaming. Para node.js e a web.

O WebTorrent é um cliente de streaming torrent para node.js e o navegador . Sim, isso é certo. O “navegador”. Está escrito completamente em JavaScript - a linguagem da web - para que o mesmo código funcione em ambos os tempos de execução[1].

Em node.js, esse módulo é um cliente de torrent simples, usando TCP e UDP para conversar com outros clientes de torrent. No navegador, o WebTorrent usa WebRTC (canais de dados) para o transporte ponto a ponto. Ele pode ser usado sem plug-ins, extensões ou instalações do navegador[1].

# O que é o React?

O React (às vezes denominado React.js ou ReactJS) é uma biblioteca  JavaScript de código aberto para criar interfaces de usuário. É mantido pelo Facebook, Instagram e uma comunidade de desenvolvedores individuais e outras empresas[2].


# O que é o WebRTC?

WebRTC é uma API em desenvolvimento elaborada pela World Wide Web Consortium (W3C) para permitir aos navegadores executar aplicações de chamada telefônica, video chat e compartilhamento P2P sem a necessidade de plugins[3].


# Arquitetura Peer-to-peer(P2P)
 
Peer-to-peer (do inglês par-a-par ou simplesmente ponto-a-ponto, com sigla P2P) é uma arquitetura de redes de computadores onde cada um dos pontos ou nós da rede funciona tanto como cliente quanto como servidor, permitindo compartilhamentos de serviços e dados sem a necessidade de um servidor central. Esse tipo de arquitetura de rede é muito conhecida pelo compartilhamento de arquivos. No entanto as redes P2P são utilizadas para outras áreas, tais como, armazenamento distribuído em meios acadêmico e científico e telecomunicações, por exemplo[4].
Assim se comparamos a arquitetura P2P e cliente/servidor: Os sistemas cliente-servidor tradicionais gerenciam e fornecem acesso a recursos como arquivos, páginas Web ou outros objetos localizados em um único computador servidor. Nesses projetos centralizados são exigidas poucas decisões sobre a distribuição dos recursos ou sobre o gerenciamento dos recursos de hardware. Os sistemas peer-to-peer fornecem acesso a recursos de informação localizados em computadores de toda a rede[4].

# Desenvolvimento de um app  webtorrent com react js

Este é um exemplo muito básico de como usar webtorrent com react js. 
Este projeto foi criado com create-react-app.

1. Passo: precisamos criar um App utilizando Node.js e VisualStudio Code. Certifique de que o node.js consta instalado na sua máquina. Create React App é um ambiente confortável para aprender React, e é a melhor maneira de começar um single-page application em React.

Além de configurar seu ambiente de desenvolvimento para utilizar as funcionalidades mais recentes do JavaScript, ele fornece uma experiência de desenvolvimento agradável, e otimiza o seu app para produção[5]. 

Digite os seguintes comandos ilustrados na figura a seguir:

Observe que o primeiro comando você criar seu App.
No segundo você entrar na pasta onde foi criada seu projeto.
Terceiro comado é só inicia pelo npm start

link de acesso: http://localhost:3000/ 

2. Passo: Assim que criado o projeto você só vai precisar editar o arquivo index.js localizado no seguinte caminho: src/index.js

No início do arquivo importe WebTorrent

```sh
import WebTorrent from "webtorrent";
```

3. Passo: Agora dentro da function App(), crie uma variavel client  que recebe o cliente ( WebTorrent) no navegador.

```sh
var client = new WebTorrent();
```

4. Passo: Crie também um variavel para receber um arquivo BitTorrent. Neste exemplo já inclui um arquivo BitTorrent, disponível na internet.
Obs.: exemplo endereco do torrent sendo qualquer um..

```sh
var torrentId = "magnet:?xt=urn:btih:endereco do torrent";
```

5. Passo: Esse trecho de código vai pegar o arquivo BitTorrent e compilar no Lado cliente WebTorrent de seu Navegador,
portanto adicione também. Torrents podem conter muitos arquivos. Vamos usar o arquivo .mp4

```sh
client.add(torrentId, function(torrent) {
// Torrents can contain many files. Let's use the .mp4 file
var file = torrent.files.find(function(file) {
return file.name.endsWith(".mp4");
});
```

6. Passo: Adicione esse trecho de código exibe o arquivo adicionando-o ao DOM. Suporta arquivos de vídeo, áudio, imagem, etc.

```sh
file.appendTo("body");
```

7. Passo: Esse trecho do código e feito de maneira natural se você deseja r pode acrescentar textos.

```sh
return (
<div className="App">
<h1>Webtorrent com react</h1>
<h2>Assistindo video com webtorrent!</h2>
</div>
);
```

8. Passo: Agora e só iniciar pelo comado npm start ou se você digitou anteriormente e só atulaiza a página do seu navegando  pelo endereço abaixo. App no react usando o WebTorrent.

```sh
http://localhost:3000/
```

# Conclusão

Um objetivo do WebTorrent é manter a compatibilidade com o BitTorrent tanto quanto possível. O WebTorrent usa o mesmo protocolo que o BitTorrent, 
mas usa um transporte diferente . O WebTorrent depende principalmente de conexões WebRTC , enquanto o BitTorrent usa conexões TCP e datagramas
UDP diretamente[7].

# Referências

1. https://github.com/webtorrent/webtorrent
2. https://pt.wikipedia.org/wiki/React_(JavaScript)
3. https://pt.wikipedia.org/wiki/WebRTC 
4. https://pt.wikipedia.org/wiki/Peer-to-peer 
5. https://pt-br.reactjs.org/docs/create-a-new-react-app.html
6. https://github.com/ungoldman/magnet-link
7. https://en.wikipedia.org/wiki/WebTorrent
 












