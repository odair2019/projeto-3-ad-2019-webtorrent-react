import React from 'react';
import './App.css';
import ReactDOM from "react-dom";
import WebTorrent from "webtorrent";

import "./styles.css";
// Odair Xavier da silva
//https://gitlab.com/odair2019/projeto-3-ad-2019-webtorrent-react
function App() {
  var client = new WebTorrent();

  var torrentId =
    "magnet:?xt=urn:btih:08ada5a7a6183aae1e09d831df6748d566095a10&dn=Sintel&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel.torrent";

  client.add(torrentId, function(torrent) {
    // Torrents podem conter muitos arquivos. Vamos usar o arquivo .mp4
    var file = torrent.files.find(function(file) {
      return file.name.endsWith(".mp4");
    });

    // Exibe o arquivo adicionando-o ao DOM. Suporta arquivos de vÃ­deo, Ã¡udio, imagem, etc.
    file.appendTo("body");
  });
  return (
    <div className="App">
      <h1>WebTorrent React</h1>
      <h2>Assitindo Video compartilhado no WebTorrent!</h2>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);

export default App;
